from socket import *
msg = """\r\nFrom: "Adam Reed" <blackfistgames@gmail.com>
To: Adam Reed <areed@csus.edu>
Date: Wed, 27 Nov 2019 01:27:43 -0500
Subject: Test Message

Hello WOrld! This is a test!
YOur Friend, big boi\r\n
"""

endmsg = "\r\n.\r\n"
heloCommand = 'HELO Adam\r\n'

# Choose a mail server (e.g. Google mail server) and call it mailserver
gSMTP = "smtp.csus.edu"
port = 25

dPckt = 'DATA\r\n'
qPckt = 'QUIT\r\n'

mailFrom = 'MAIL FROM:<blackfistgames@gmail.com>\r\n'
mailTo   = 'RCPT TO:<areed@csus.edu>\r\n'


# Create socket called clientSocket and establish a TCP connection with mailserver
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((gSMTP, port))

recv = clientSocket.recv(1024)
print(recv)
    
# Send HELO command and print server response.

clientSocket.send(heloCommand.encode())
print("Sending HELO")
recv1 = clientSocket.recv(1024)
print(recv1)

# Send MAIL FROM command and print server response.
clientSocket.send(mailFrom.encode())
print("Sending mailFrom...")
recv1 = clientSocket.recv(1024)
print(recv1)
    
# Send RCPT TO command and print server response.
clientSocket.send(mailTo.encode())
print("Sending MAILTO...")
recv1 = clientSocket.recv(1024)
print(recv1)
    
    
# Send DATA command and print server response.
clientSocket.send(dPckt.encode())
print("Sending DATA...")
recv1 = clientSocket.recv(1024)
print(recv1)
    
# Send message data.
print("Sending msg...")
clientSocket.send(msg.encode())

# Message ends with a single period.
clientSocket.send(endmsg.encode())
print("Sending ENd Message...")
recv1 = clientSocket.recv(1024)
print(recv1)

# Send QUIT command and get server response.
clientSocket.send(qPckt.encode())
print("Sending Quit....")
recv1 = clientSocket.recv(1024)
print(recv1)
