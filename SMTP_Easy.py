import smtplib

fromAddr = "address@gmail.com"
toAddr = "emailAddress@address.com"

msg = """Subject: Hello World!
Testing Testing 123 Have a nice day!"""

server = smtplib.SMTP("smtp.gmail.com", 587)
server.starttls()

server.login(fromAddr, "password" )

server.sendmail(fromAddr, toAddr, msg)

server.quit()
